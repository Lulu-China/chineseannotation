.. LuluBook documentation master file, created by
   sphinx-quickstart on Fri Nov  2 21:32:39 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Lulu's documentation!
====================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    语料处理1——文本预处理.md
    语料处理2__标注任务分配.md
    语料处理3__一致率比较.md
    pyltp语料预处理操作笔记.md
    大规模新闻语料预处理.md

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
