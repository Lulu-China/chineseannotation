### pyltp操作笔记

#### 一、安装pyltp模块

> 参考方法：[https://www.mlln.cn/2018/01/31/pyltp在windows下的编译安装/](url)

踩过的坑：windows系统中，目前不能是python3.7，即便用参考链接的第一种方法，也安装不上，大神也许可以。

#### 二、分句、分词、词性标注体验

1. 存在问题：
   一些英文单词兼容性有待观察，分句精度根据文本内容不可控。
   in 和 and 不能在同一句，如果在同一句，会将and后面的内容分成另外一行。and 似乎与 output 在同一行也不兼容，会在output后断行。
2. 分句后损失段落信息，没有将段落与段落之间的分隔与句子之间的分割区分开来。也未对每句进行编号。
3. 标句点号设置不合理。根据五种中文标点符号。？！；…… 其中省略号作为分句的形式标志，未考虑到省略号表示说话断断续续的情况。强行分句，不合理。
4. 分词的特殊符号结合的词（类似I/0、3:00）会分开。个性化分词也许可以解决这个问题

结论：
		1.能满足标准文本（如《人民日报》）的绝大多数分句要求，但对于非标准文本分句结果不可控。
		2.NLP相关编程计算，最好要有语言学理论指导，考虑问题才全面，结果有更好的实用性。

#### 三、各模块练习

##### 1.分句

> 代码参考：[https://blog.csdn.net/churximi/article/details/51173297](url)

```python
import codecs 
from pyltp import SentenceSplitter
 
file = open("D:\工作\segsentence.txt","r")
savef = codecs.open("D:\\工作\\out1.txt", "a", "utf-8")

#将文件分句
while 1:
	lines = file.readlines()
	if not lines:
		break
	for line in lines:
		sents = SentenceSplitter.split(line)
		# print('\n'.join(sents))
		savef.write('\n'.join(sents))
file.close()
savef.close()
```

##### 2.分词

> 代码参考：[https://pyltp.readthedocs.io/zh_CN/latest/api.html#id13](url)

```python
import codecs
import pyltp
import os
Model_Path="D:\\Program Files (x86)\\ltp_data_v3.4.0"
cws_model_path = os.path.join(Model_Path,'cws.model')

from pyltp import Segmentor
segmentor = Segmentor()
segmentor.load(cws_model_path)

file = open("D:\工作\out1.txt","rb")
savef = codecs.open("D:\\工作\\out2.txt", "a", "utf-8")
while 1:
	lines = file.readlines()
	if not lines:
		break
	for line in lines:
		words=segmentor.segment(line)
		# print ('\t'.join(words))
		savef.write('\t'.join(words))
		savef.write('\n')
	segmentor.release()
file.close()
savef.close()		
```

##### 3.词性标注

> 参考代码：[https://blog.csdn.net/baidu_15113429/article/details/78909666](url)

```python
import codecs
import pyltp
import os

from pyltp import Postagger
def posttagger(words):
	postagger = Postagger()
	postagger.load("D:/Program Files (x86)/ltp_data_v3.4.0/pos.model")
	posttags = postagger.postag(words)  #词性标注
	postags = list(posttags)
	postagger.release()  #释放模型
	return postags
file=open("D:\工作\segword\ceshi_word.txt",'r',encoding='utf8')
outfile=open("D:\\工作\\tagword\\ceshi_tag.txt",'w',encoding='utf8')

while 1:
	lines = file.readlines()
	if not lines:
		break
	for line in lines:
		words = line.split( )
		postags = posttagger(words)
		for postag, word in zip(postags, words):
			outfile.write(word + "/" + postag+" ")
			# print(word + " / " + postag)
		outfile.write("\n")
outfile.close()
file.close()
```

#### 四、3项功能封装为函数
##### 1.功能分块函数

> - 以下函数是对分句、分词、词性标注的函数封装，目的在于能直接使用函数批量处理文本，最终完成语料预处理。
> - 编码是最大的坑，需要注意读写时的编码，否则编译不过去。
> - 如果按照这个顺序依次执行产生的文件大小，每次几乎都翻倍。词性标注的速度远比分句与分词的速度慢，当数据大小翻倍后，运行时间，将很长，所以要根据需求合并和选择代码。
> - 写完这三个函数就对python的一些基本语法以及常用操作熟悉了。

```python
import codecs 
import os
import re
import pyltp
import sys
from pyltp import SentenceSplitter
from pyltp import Segmentor
from pyltp import Postagger
from os import listdir
from os.path import isfile, join
	
def SegSentence(Path_Open_File,Path_Write_File): 
	file = open(Path_Open_File,"rb")
	savef = codecs.open(Path_Write_File, "w", "utf-8")
	# print (savef)
	
	#将文件分句
	while 1:
		lines = file.readlines()
		if not lines:
			break
		for line in lines:
			sents = SentenceSplitter.split(line)
			# print('\n'.join(sents))
			savef.write('\n'.join(sents))
	file.close()
	savef.close()
	return()
	
def SegWord(Model_Path,cws_model_path,Path_Open_File,Path_Write_File):

	segmentor = Segmentor()
	segmentor.load(cws_model_path)

	file = open(Path_Open_File,"rb")
	# print(file)
	savef = codecs.open(Path_Write_File, "a", "utf-8")
	
	while 1:
		lines = file.readlines()
		if not lines:
			break
		for line in lines:
			words=segmentor.segment(line)
			# print ('\t'.join(words))
			savef.write('\t'.join(words))
			savef.write('\n')
		segmentor.release()
	file.close()
	savef.close()
	return()	
	
def tagger(words):
	tagger = Postagger() 
	tagger.load(pos_model_path)
	posttags = tagger.postag(words)  #词性标注
	postags = list(posttags)
	tagger.release()  #释放模型
	return postags
def Addtagger(Model_Path,pos_model_path,Path_Open_File,Path_Write_File):
    id =0
    number = 0
    file = open(Path_Open_File,"r",encoding='utf-8')
    savef = open(Path_Write_File, "w", encoding="utf-8")
    
    lines = file.readlines()
    write_content = ''
    for line in lines:
        words = line.split( )
        postags = tagger(words)
        for postag, word in zip(postags, words):#把词和词性标注合在一起：我/np 吃/v 饭/n
            word = str(word)#zip函数需要两个参数数据类型一样，word为字节，需要转换为字符。
            write_content = write_content+word+'/'+postag + " "
            
        write_content = write_content+'\n'
        write_content = write_content.encode("utf-8").decode("utf-8")
        if(savef.write(write_content)):
            id = id + 1
            if(id > 100):
                number = number+1
                print(number)#输出数字，监测程序输出运行情况
                id = 0
            
    savef.close()
    file.close()
	
Model_Path="D:\\Program Files (x86)\\ltp_data_v3.4.0"
pos_model_path = os.path.join(Model_Path,'pos.model')
word_Path_Open_File = "D:\\工作\\test"
Temp_Path_Write_File = "D:\\工作\\testTag"

word_files = [f for f in listdir(word_Path_Open_File) if isfile(join(word_Path_Open_File, f))]
# print(word_files)
for wfile in word_files:
	wPath_Open_File = os.path.join(word_Path_Open_File,wfile)
	# print(wPath_Open_File)
	matchObj = re.match(r'(.*)_word\.txt',wfile,re.M|re.I)
	# print(matchObj)
	wfname = matchObj.group(1)
	# print(wfname)
	wPath_Write_File = Temp_Path_Write_File+"\\"+wfname+"_tag.txt"
	# print(wPath_Write_File)
	Addtagger(Model_Path,pos_model_path,wPath_Open_File,wPath_Write_File)

'''
1.调用SegSentence分句
'''
Path = "D:\\工作"
Seg_Sentence_Path = "D:\\工作\\segsentence"

rawfiles = [f for f in listdir(Path) if isfile(join(Path, f))]

for file in rawfiles:
	Path_Open_File = os.path.join(Path,file)
	matchObj = re.match(r'(.*)\.txt',file,re.M|re.I)
	fname = matchObj.group(1)
	Path_Write_File = Seg_Sentence_Path+"\\"+fname+"_sentence.txt"
	# print(Path_Write_File)
	# SegSentence(Path_Open_File,Path_Write_File) #调用的时候放开该分句函数

'''
2.调用SegWord分词
'''
Model_Path="D:\\Program Files (x86)\\ltp_data_v3.4.0"
cws_model_path = os.path.join(Model_Path,'cws.model')
Sentence_Path_Open_File = "D:\工作\segsentence"
Tem_Path_Write_File = "D:\\工作\\segword"

sent_files = [f for f in listdir(Sentence_Path_Open_File) if isfile(join(Sentence_Path_Open_File, f))]

for file in sent_files:
	SPath_Open_File = os.path.join(Sentence_Path_Open_File,file)
	matchObj = re.match(r'(.*)_sentence\.txt',file,re.M|re.I)
	sfname = matchObj.group(1)
	SPath_Write_File = Tem_Path_Write_File+"\\"+sfname+"_word.txt"
	# print(SPath_Write_File)
	# SegWord(Model_Path,cws_model_path,SPath_Open_File,SPath_Write_File)#调用的时候放开该分词函数
	

'''
3.调用Postagger标注词性
'''

Model_Path="D:\\Program Files (x86)\\ltp_data_v3.4.0"
pos_model_path = os.path.join(Model_Path,'pos.model')
word_Path_Open_File = "D:\\工作\\segword"
Temp_Path_Write_File = "D:\\工作\\tagword"

word_files = [f for f in listdir(word_Path_Open_File) if isfile(join(word_Path_Open_File, f))]
# print(word_files)
for wfile in word_files:
	wPath_Open_File = os.path.join(word_Path_Open_File,wfile)
	matchObj = re.match(r'(.*)_word\.txt',wfile,re.M|re.I)
	wfname = matchObj.group(1)
	wPath_Write_File = Temp_Path_Write_File+"\\"+wfname+"_tag.txt"
	# print(wPath_Write_File)
	Addtagger(Model_Path,pos_model_path,wPath_Open_File,wPath_Write_File)#调用的时候放开该分词函数
```
##### 2.分词、词性标注合一函数

​	上面的Addtagger函数，在运用zip、str合并词和词性时，运算非常大，但如果拿到的是分词文本，要进行词性标注，是一种可参考的方式。如果是非分词文本，可以词性标注和分词同时进行，可以减少运算量和数据量。

```python
import codecs 
import os
import re
import pyltp
import sys

def Seg_Addtagger(Model_Path,cws_model_path,pos_model_path,Path_Open_File,Path_Write_File):
	segmentor = Segmentor()  # 初始化实例
	segmentor.load(cws_model_path)  # 加载模型
	tagger = Postagger() 
	tagger.load(pos_model_path)
	
	File = open(Path_Open_File,"r",encoding='utf-8')
	savef = open(Path_Write_File, "w", encoding="utf-8")
	
	lines = File.readlines()
	for line in lines:
		line = line.strip()
		words = segmentor.segment(line)#分词
		words_list = list(words)
		posttags = tagger.postag(words_list)  #词性标注
		postags_list = list(posttags)
		temp_list =[]
		for i in range(0,len(words_list)):
			temp = words_list[i]+'/'+postags_list[i]
			temp_list.append(temp)
			# print (temp_list)
			seg = ' '.join(temp_list)
			savef.write (seg)

	savef.close()
	File.close()
	tagger.release()  #释放模型

Model_Path="D:\\Program Files (x86)\\ltp_data_v3.4.0"
cws_model_path = os.path.join(Model_Path,'cws.model')
pos_model_path = os.path.join(Model_Path,'pos.model')

word_Path_Open_File = "D:\\工作\\test"
Temp_Path_Write_File = "D:\\工作\\testTag"

word_files = [f for f in listdir(word_Path_Open_File) if isfile(join(word_Path_Open_File, f))]
# print(word_files)
for wfile in word_files:
	wPath_Open_File = os.path.join(word_Path_Open_File,wfile)
	# print(wPath_Open_File)
	matchObj = re.match(r'(.*)_sentence\.txt',wfile,re.M|re.I)
	# print(matchObj)
	wfname = matchObj.group(1)
	# print(wfname)
	wPath_Write_File = Temp_Path_Write_File+"\\"+wfname+"_tag.txt"
	# print(wPath_Write_File)
	Seg_Addtagger(Model_Path,cws_model_path,pos_model_path,wPath_Open_File,wPath_Write_File)
```


